Ext.define('MisNotas.model.Nota', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'id', type: 'int'},
      {name: 'date', type: 'date', dateFormat: 'c'},
      {name: 'title', type: 'string'},
      {name: 'text', type: 'string'},
    ],
    validations: [
      {type: 'presence', field: 'id'},
      {type: 'presence', field: 'title', message: 'Introduzca un título'},
      {type: 'presence', field: 'text', message: 'Introduzca un texto'}
    ]
  }
})
