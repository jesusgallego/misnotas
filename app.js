//---------------------------------------------------------------------------
// Mis Notas
﻿//---------------------------------------------------------------------------
var panelPrincipal = null;
var panelAyuda = null;
var panelFormulario = null;
var panelContenedorLista = null;
var listaNotas = null;

var storeNotas = null;

//---------------------------------------------------------------------------
// Modelo y Store

/*Ext.define('MisNotas.model.Nota', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'id', type: 'int'},
      {name: 'date', type: 'date', dateFormat: 'c'},
      {name: 'title', type: 'string'},
      {name: 'text', type: 'string'},
    ],
    validations: [
      {type: 'presence', field: 'id'},
      {type: 'presence', field: 'title', message: 'Introduzca un título'},
      {type: 'presence', field: 'text', message: 'Introduzca un texto'}
    ]
  }
})*/

function crearStore() {
  storeNotas = Ext.create('Ext.data.Store', {
    model: 'MisNotas.model.Nota',
    proxy: {
      type: 'localstorage',
      id: 'misNotas-app-localstore2'
    },
    /*data: [
      { id: 1, date: new Date(), title: 'Test 1', text: 'texto de prueba' },
      { id: 2, date: new Date(), title: 'Test 2', text: 'texto de prueba' },
      { id: 3, date: new Date(), title: 'Test 3', text: 'texto de prueba' },
      { id: 4, date: new Date(), title: 'Test 4', text: 'texto de prueba' }
    ],*/
    sorters: [
      {property: 'date', direction: 'DESC'}
    ],
    autoLoad: true
  });

  storeNotas.add({ id: 1, date: new Date(), title: 'Test 1', text: 'texto de prueba' },
  { id: 2, date: new Date(), title: 'Test 2', text: 'texto de prueba' },
  { id: 3, date: new Date(), title: 'Test 3', text: 'texto de prueba' },
  { id: 4, date: new Date(), title: 'Test 4', text: 'texto de prueba' });
}

﻿//---------------------------------------------------------------------------
// PANEL AYUDA
function buildPanelAyuda()
{
  const topToolbar = Ext.create('Ext.Toolbar', {
    docked: 'top',
    title: 'Ayuda',
    items: [
      {
        xtype: 'button',
        ui: 'normal',
        iconCls: 'home',
        handler: () => {
          panelPrincipal.animateActiveItem(panelContenedorLista,{
            type: 'slide',
            duration: 400,
            direction: 'up',
          });
        }
      }
    ]
  })

  const htmlAyuda1 = `
    <div class="ayuda">
      <h1>Mis Notas</h1>
      <p>Aplicación Web para la gestión de notas realizada con Sencha Touch. </p>
      <p>Máster en Desarrollo de Aplicaciones para Dispositivos Móviles</p>
      <img src="resources/imgs/logo.png" alt="Sencha logo" />
    </div>
  `

  const htmlAyuda2 = `
  <div class="ayuda">
    <h1>Autor</h1>
    <p>Jesús Gallego Irles</p>
    <p>Universidad de Alicante</p>
    <img src="https://avatars0.githubusercontent.com/u/7962154?v=3&s=460" alt="Sencha logo" />
  </div>
  `

	panelAyuda = Ext.create('Ext.Carousel', {
    fullScreen: true,
    defaults: {
      styleHtmlContent: true
    },
    items: [
      topToolbar,
      {html: htmlAyuda1},
      {html: htmlAyuda2}
    ]
  });

	return panelAyuda;
}


﻿//---------------------------------------------------------------------------
// PANEL FORMULARIO
function buildPanelFormulario()
{
  const topToolbar = Ext.create('Ext.Toolbar', {
    docked: 'top',
    title: 'Editar nota',
    items: [
      {
        xtype: 'button',
        ui: 'normal',
        iconCls: 'home',
        handler: () => {
          panelPrincipal.animateActiveItem(panelContenedorLista,{
            type: 'slide',
            duration: 400,
            direction: 'right',
          })
        }
      },
      {xtype: 'spacer'},
      {
        xtype: 'button',
        ui: 'action',
        text: 'Guardar',
        handler: () => {
          if (guardarNota()) {
            panelPrincipal.animateActiveItem(panelContenedorLista,{
              type: 'slide',
              duration: 400,
              direction: 'right',
            })
          }
        }
      }
    ]
  })
  const bottomToolbar = Ext.create('Ext.Toolbar', {
    docked: 'bottom',
    items: [
      {xtype: 'spacer'},
      {
        xtype: 'button',
        ui: 'decline',
        iconCls: 'trash',
        handler: () => {
          borrarNota()
          panelPrincipal.animateActiveItem(panelContenedorLista,{
            type: 'slide',
            duration: 400,
            direction: 'right',
          })
        }
      }
    ]
  })
	panelFormulario = Ext.create('Ext.form.Panel', {
    fullScreen: true,
    items: [
      topToolbar,
      bottomToolbar,
      {
        xtype: 'textfield',
        name: 'title',
        label: 'Título:',
        required: true
      },
      {
        xtype: 'textareafield',
        name: 'text',
        label: 'Texto:',
        required: true
      }
    ]
  });

	return panelFormulario;
}

function crearNuevaNota() {
  const date = new Date()
  const id = date.getTime()

  let note = Ext.create('MisNotas.model.Nota', {
    id,
    date,
    title: 'titulo',
    text: 'text'
  })

  panelFormulario.setRecord(note)
}

function guardarNota() {
  let record = panelFormulario.getRecord()

  // Actualizar instancia del formulario
  panelFormulario.updateRecord(record)
  // Validar
  let errors = record.validate()

  if (!errors.isValid()) {
    var message="";

    Ext.each( errors.items, function(item, index) {
        message += item.getMessage() + "<br/>";
    });

    Ext.Msg.alert("Errores", message, Ext.emptyFn);

    return false;   // Terminamos si hay errores
  }

  if( storeNotas.findRecord('id', record.data.id) === null){
    storeNotas.add( record );
  }

  storeNotas.sync();
  storeNotas.sort([{ property: 'date', direction: 'DESC'}]);

  listaNotas.refresh();

  return true;
}

function borrarNota() {
  let record = panelFormulario.getRecord()

  // Comprobar si existe
  if( storeNotas.findRecord('id', record.data.id)){
    storeNotas.remove( record );
  }
  storeNotas.sync()
  listaNotas.refresh()
}

﻿//---------------------------------------------------------------------------
// PANEL LISTA
function buildPanelContenedorLista()
{
  const topToolbar = Ext.create('Ext.Toolbar', {
    docked: 'top',
    title: 'Mis Notas',
    items: [
      {
        xtype: 'button',
        cls: 'btnAyuda',
        handler: () => {
          panelPrincipal.animateActiveItem(panelAyuda,{
            type: 'slide',
            duration: 400,
            direction: 'down',
          });
        }
      },
      {xtype: 'spacer'},
      {
        xtype: 'button',
        ui: 'action',
        text: 'Nueva Nota',
        handler: () => {
          crearNuevaNota()
          panelPrincipal.animateActiveItem(panelFormulario,{
            type: 'slide',
            duration: 400,
            direction: 'left',
          });
        }
      }
    ]
  })

  listaNotas = Ext.create('Ext.List', {
    fullScreen: true,
    store: storeNotas,
    itemTpl: `
      <div class="list-item">
        <div class="list-item-title">{title}</div>
        <div class="list-item-text">{text}</div>
      </div>
    `,
    onItemDisclosure: (record) => {
      panelFormulario.setRecord(record)
      panelPrincipal.animateActiveItem(panelFormulario,{
        type: 'slide',
        duration: 400,
        direction: 'left',
      });
    }
  })

	panelContenedorLista = Ext.create('Ext.Panel', {
    layout: 'fit',
    items: [
      topToolbar,
      listaNotas
    ]
  });

	return panelContenedorLista;
}


﻿//---------------------------------------------------------------------------
// PANEL PRINCIPAL
function buildPanelPrincipal()
{
  crearStore();

  buildPanelContenedorLista();
  buildPanelFormulario();
  buildPanelAyuda();

	panelPrincipal = Ext.create('Ext.Panel', {
    fullScreen: true,
    layout: 'card',
    items: [
      panelContenedorLista,
      panelFormulario,
      panelAyuda
    ]
  });

	return panelPrincipal;
}


﻿//---------------------------------------------------------------------------
// APLICACIÓN
Ext.application({
    name: 'MisNotas',

    models: [
      'MisNotas.model.Nota'
    ],

    requires: [
        'Ext.MessageBox', 'Ext.Toolbar', 'Ext.Panel', 'Ext.List', 'Ext.form.Panel', 'Ext.carousel.Carousel'
    ],

    launch: function() {
        Ext.fly('appLoadingIndicator').destroy();
        Ext.Viewport.add( buildPanelPrincipal() );
    }
});
